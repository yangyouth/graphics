#include <iostream.h>
class CRectangle//定义长方形类
{
public:
	CRectangle();//声明默认构造函数
	CRectangle(int width,int height);//声明带参构造函数
	~CRectangle();//声明析构函数
	double circum();//声明计算周长函数
	double area();//声明计算面积函数
private:
	int width;//声明长方形的宽度
	int height;//声明长方形的高度
};
CRectangle::CRectangle()//定义默认构造函数
{
	width=10;
	height=5;	
	cout<<"建立默认对象"<<endl;
}
CRectangle::CRectangle(int width,int height)//定义带参构造函数
{
	this->width=width;
	this->height=height;	
	cout<<"建立对象"<<endl;
}
CRectangle::~CRectangle()//定义析构函数
{
	cout<<"撤销对象"<<endl;
}
double CRectangle::circum()//定义计算周长函数
{
	return 2*(width+height);
}
double CRectangle::area()                       //定义计算面积函数
{
	return width*height; 
}
void main()                                     //主函数
{
	int RowIndex=2,ColIndex=3;//设置二维数组行下标和列下标
	CRectangle **pRect;
	//创建二维动态数组
    pRect=new CRectangle *[RowIndex]; //设置行
	for(int i=0;i<RowIndex;i++)
	{
        pRect[i]=new CRectangle[ColIndex];//设置列
	}	
	//输出周长和面积
	for(i=0;i<RowIndex;i++)
	{
		for(int j=0;j<ColIndex;j++)
		{
			cout<<"Rect["<<i<<","<<j<<"]的周长为"<<pRect[i][j].circum()<<","; 
			cout<<"面积为"<<pRect[i][j].area()<<endl; 
		}
	}
	//释放二维动态数组
	for(int k=0;k<RowIndex;k++)
	{
		delete []pRect[k];//注意撤销次序,先列后行,与设置相反
		pRect[k]=NULL;
	}
	delete []pRect;
	pRect=NULL;
}