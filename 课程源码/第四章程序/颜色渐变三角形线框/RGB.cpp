// RGB.cpp: implementation of the CRGB class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Test.h"
#include "RGB.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRGB::CRGB()
{

}

CRGB::~CRGB()
{

}
CRGB::CRGB(double red,double green,double blue)
{
	this->red =red;
	this->green =green;
	this->blue =blue;
}
CRGB operator *(double k ,const CRGB &c0)
{
	CRGB c;
	c.red=k*c0.red;
	c.green=k*c0.green ;
	c.blue =k*c0.blue ;
	return c;

}
CRGB operator +( const CRGB &c1,const CRGB &c2)
{
	CRGB c;
	c.red=c1.red +c2.red ;
	c.green =c1.green +c2.green ;
	c.blue =c1.blue +c2.blue ;
	return c;
}