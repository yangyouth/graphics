// Transform2.h: interface for the CTransform2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRANSFORM2_H__DE784F80_DE06_4221_89E7_D678B91ABE9D__INCLUDED_)
#define AFX_TRANSFORM2_H__DE784F80_DE06_4221_89E7_D678B91ABE9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "P2.h"

class CTransform2  
{
public:
	CTransform2();
	virtual ~CTransform2();
	void SetMat(CP2 *,int);
	void Identity();
	void Translate(double,double);//平移变换矩阵
	void Scale(double,double);//比例变换矩阵
	void Scale(double,double,CP2);//相对于任意点的比例变换矩阵
	void Rotate(double);//旋转变换矩阵
	void Rotate(double,CP2);//相对于任意点的旋转变换矩阵
	void ReflectO();//原点反射变换矩阵
	void ReflectX();//X轴反射变换矩阵
	void ReflectY();//Y轴反射变换矩阵
	void Shear(double,double);//错切变换矩阵
	void MultiMatrix();//矩阵相乘
public:
	double T[3][3]; //变换矩阵
	CP2 *POld;  //变换的顶点数据
	int num;  //变换的顶点个数
};

#endif // !defined(AFX_TRANSFORM2_H__DE784F80_DE06_4221_89E7_D678B91ABE9D__INCLUDED_)
