// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here

}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	Line(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers
void CTestView::Line(CDC *pDC)
{
	//斜率为【0,1】之间直线
	CPoint p0(-100,-50),p1(200,50),p;
	int dx,dy;
	dx=p1.x-p0.x;
	dy=p1.y-p0.y;
	double k,d;
	k=(double)dy/dx;
	d=0.5-k; 
    for(p=p0;p.x<p1.x;p.x++)//不包括终点p1
	{
		pDC->SetPixelV(p,RGB(0,0,0));
        if(d<0)
		{
			p.y++;
			d+=1-k;
		}
		else 
			d-=k;
	}

	//通过对称法显示斜率为【1，infinity】的直线
	//	CPoint p0(-100,-50),p1(200,50),p;
/*	CPoint p0(-50,-100),p1(50,200),p;

	CPoint tp0,tp1;
	tp0.x = p0.y; tp0.y = p0.x;
	tp1.x = p1.y; tp1.y = p1.x;

	p0 = tp0;
	p1 = tp1;

	int dx,dy;
	dx=p1.x-p0.x;
	dy=p1.y-p0.y;
	double k,d;
	k=(double)dy/dx;
	d=0.5-k; 

	CPoint temp;

    for(p=p0;p.x<p1.x;p.x++)//不包括终点p1
	{
		temp.x = p.y;
		temp.y = p.x;

	//	pDC->SetPixelV(p,RGB(0,0,0));
		pDC->SetPixelV(temp,RGB(0,0,0));
        if(d<0)
		{
			p.y++;
			d+=1-k;
		}
		else 
			d-=k;
	}*/
}
