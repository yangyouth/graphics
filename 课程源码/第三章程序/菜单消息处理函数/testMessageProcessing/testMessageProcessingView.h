// testMessageProcessingView.h : interface of the CTestMessageProcessingView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTMESSAGEPROCESSINGVIEW_H__2EEA5210_03F8_4C78_9F7F_E7F306010E5C__INCLUDED_)
#define AFX_TESTMESSAGEPROCESSINGVIEW_H__2EEA5210_03F8_4C78_9F7F_E7F306010E5C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTestMessageProcessingView : public CView
{
protected: // create from serialization only
	CTestMessageProcessingView();
	DECLARE_DYNCREATE(CTestMessageProcessingView)

// Attributes
public:
	CTestMessageProcessingDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestMessageProcessingView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTestMessageProcessingView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTestMessageProcessingView)
	afx_msg void OnMdraw();
	afx_msg void OntopMenu();
	afx_msg void OnNewPopMenu();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void MBCircle(int R,CDC *pDC);
	void CirclePoint(int x, int y,CDC *pDC);
};

#ifndef _DEBUG  // debug version in testMessageProcessingView.cpp
inline CTestMessageProcessingDoc* CTestMessageProcessingView::GetDocument()
   { return (CTestMessageProcessingDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTMESSAGEPROCESSINGVIEW_H__2EEA5210_03F8_4C78_9F7F_E7F306010E5C__INCLUDED_)
