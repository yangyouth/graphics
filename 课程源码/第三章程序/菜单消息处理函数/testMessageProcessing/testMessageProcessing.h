// testMessageProcessing.h : main header file for the TESTMESSAGEPROCESSING application
//

#if !defined(AFX_TESTMESSAGEPROCESSING_H__B7752B04_1D1D_4E27_AA25_67686F7BC75C__INCLUDED_)
#define AFX_TESTMESSAGEPROCESSING_H__B7752B04_1D1D_4E27_AA25_67686F7BC75C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingApp:
// See testMessageProcessing.cpp for the implementation of this class
//

class CTestMessageProcessingApp : public CWinApp
{
public:
	CTestMessageProcessingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestMessageProcessingApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTestMessageProcessingApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTMESSAGEPROCESSING_H__B7752B04_1D1D_4E27_AA25_67686F7BC75C__INCLUDED_)
