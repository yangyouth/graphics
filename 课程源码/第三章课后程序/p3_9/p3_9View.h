// p3_9View.h : interface of the CP3_9View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_9VIEW_H__BCD96931_26FF_4FB4_A2C9_C9868055B19C__INCLUDED_)
#define AFX_P3_9VIEW_H__BCD96931_26FF_4FB4_A2C9_C9868055B19C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_9View : public CView
{
protected: // create from serialization only
	CP3_9View();
	DECLARE_DYNCREATE(CP3_9View)

// Attributes
public:
	CP3_9Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_9View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_9View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_9View)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in p3_9View.cpp
inline CP3_9Doc* CP3_9View::GetDocument()
   { return (CP3_9Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_9VIEW_H__BCD96931_26FF_4FB4_A2C9_C9868055B19C__INCLUDED_)
