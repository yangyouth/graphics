// Circle.h: interface for the CCircle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CIRCLE_H__B6291A75_D0AC_4F74_B85B_F379FE615AD8__INCLUDED_)
#define AFX_CIRCLE_H__B6291A75_D0AC_4F74_B85B_F379FE615AD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCircle  
{
public:
	CCircle();
	void drawCirclep(CDC * pDC,double m,double n,double r);
	void drawCircle(CDC * pDC,double m,double n,double r);
	void drawCirclep2(CDC * pDC,double m,double n,double r);
	virtual ~CCircle();

};

#endif // !defined(AFX_CIRCLE_H__B6291A75_D0AC_4F74_B85B_F379FE615AD8__INCLUDED_)
