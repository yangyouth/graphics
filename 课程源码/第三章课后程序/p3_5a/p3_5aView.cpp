// p3_5aView.cpp : implementation of the CP3_5aView class
//

#include "stdafx.h"
#include "p3_5a.h"
#include "Circle.h"
#include "p3_5aDoc.h"
#include "p3_5aView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView

IMPLEMENT_DYNCREATE(CP3_5aView, CView)

BEGIN_MESSAGE_MAP(CP3_5aView, CView)
	//{{AFX_MSG_MAP(CP3_5aView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView construction/destruction

CP3_5aView::CP3_5aView()
{
	// TODO: add construction code here

}

CP3_5aView::~CP3_5aView()
{
}

BOOL CP3_5aView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView drawing

void CP3_5aView::OnDraw(CDC* pDC)
{
	CP3_5aDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);

	CCircle c;
	c.drawCircle(pDC,-100,-100,100.00);
}

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView printing

BOOL CP3_5aView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CP3_5aView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CP3_5aView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView diagnostics

#ifdef _DEBUG
void CP3_5aView::AssertValid() const
{
	CView::AssertValid();
}

void CP3_5aView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CP3_5aDoc* CP3_5aView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CP3_5aDoc)));
	return (CP3_5aDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP3_5aView message handlers
